﻿using System.Collections.Generic;
using System.Linq;
using ChipoApplication;
using Microsoft.Office.Interop.Word;

namespace CombinarArchivosWord
{
    public static class CombinarArchivos
    {       
        
        public static void Combinar(IEnumerable<string> rutaArchivos, string rutaNombreArchivoCombinado)
        {
            object missing = System.Reflection.Missing.Value;
            _Application oWord = new Application();
            _Document oDoc = oWord.Documents.Add();
            var oSelection = oWord.Selection;
            
            foreach (string documentFile in rutaArchivos)
            {
                _Document oCurrentDocument = oWord.Documents.Add(documentFile);
                CopiarConfiguracionHoja(oCurrentDocument.PageSetup, oDoc.Sections.Last.PageSetup);
                oCurrentDocument.Range().Copy();
                oSelection.PasteAndFormat(WdRecoveryType.wdFormatOriginalFormatting);
                if (!ReferenceEquals(documentFile, rutaArchivos.Last()))
                    oSelection.InsertBreak(WdBreakType.wdSectionBreakNextPage);
            }

            oDoc.SaveAs(rutaNombreArchivoCombinado);
           
            oDoc.Close(ref missing, ref missing, ref missing);
            oWord.Quit(ref missing, ref missing, ref missing);

            Compartidas.LiberarObjetos(oWord);
            Compartidas.LiberarObjetos(oDoc);
            Compartidas.LiberarObjetos(oSelection);
        }

        private static void CopiarConfiguracionHoja(PageSetup paginaOrigen, PageSetup paginaDestino)
        {
            paginaDestino.PaperSize = paginaOrigen.PaperSize;

            if (!paginaOrigen.Orientation.Equals(paginaDestino.Orientation))
                paginaDestino.TogglePortrait();

            paginaDestino.TopMargin = paginaOrigen.TopMargin;
            paginaDestino.BottomMargin = paginaOrigen.BottomMargin;
            paginaDestino.RightMargin = paginaOrigen.RightMargin;
            paginaDestino.LeftMargin = paginaOrigen.LeftMargin;
            paginaDestino.FooterDistance = paginaOrigen.FooterDistance;
            paginaDestino.HeaderDistance = paginaOrigen.HeaderDistance;
            paginaDestino.LayoutMode = paginaOrigen.LayoutMode;
        }
       
    }
        
}
