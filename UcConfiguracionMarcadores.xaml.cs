﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ChipoApplication.Controlador;
using ChipoApplication.EntidadesChipo;
using System.Collections.Generic;

namespace ChipoApplication
{
    /// <summary>
    /// Interaction logic for UcConfiguracionMarcadores.xaml
    /// </summary>
    public partial class UcConfiguracionMarcadores : UserControl
    {

        #region Atributos

        private Base _datosApp;     
        private CtrlChipo _controladorApp;
        private CtrlConfiguracion _controlador;

        #endregion
        #region Constructores

        public UcConfiguracionMarcadores()
        {
            InitializeComponent();            
        }

        #endregion
        #region Propiedades
        
        #endregion
        #region Eventos

        //evento para el cierre del modulo de Configuración
        public event EventHandler CerrarConfiguracion;
        //evento para avisar que la Configuración ha sigo actualizada
        public event EventHandler ConfiguracionActualizada;

        #endregion
        #region Metodos
        
        private void AgregarDelegados()
        {
            btnAceptar.Click += btnAceptar_Click;
            btnCerrar.Click += btnCerrar_Click;
        }

        /// <summary>
        /// Método que ejecuta el evento de Cerrar la ventana de la Configuración
        /// </summary>
        private void CerrarVentanaConfiguracion()
        {
            EventHandler manejadorLocal = CerrarConfiguracion;

            if (manejadorLocal != null)
                manejadorLocal(this, null);
        }
    
        /// <summary>
        /// Método que permite verificar si los datos configurados son validos
        /// </summary>
        /// <returns></returns>
        private bool EsValidoDatosConfiguracion()
        {
            List<string> listaMensajesValidacion = _controlador.EsValidoDatosConfiguracion();

            if (listaMensajesValidacion.Count > 0)
            {
                foreach (string mensaje in listaMensajesValidacion)
                {
                    lblMensajes.Content = mensaje;
                }

                return false;
            }

            return true;
        }

        /// <summary>
        /// Método que guarda los valores de Configuración realizada por el usuario
        /// </summary>
        private void GuardarConfiguracion()
        {
            Cursor = Cursors.Wait;            

            //ejecuta el evento que  avisa cuando la configuración ha sido actualizada
            EventHandler manejadorLocal = ConfiguracionActualizada;

            if (manejadorLocal != null)
                manejadorLocal(this, null);

            Cursor = null;
        }


        public void Iniciar(CtrlChipo controlador)
        {
            _controladorApp = controlador;
            _controlador = new CtrlConfiguracion(_controladorApp.DatosGenerales);

            AgregarDelegados();
        }        

        #endregion
        #region Delegados

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {

            if(EsValidoDatosConfiguracion())
            {
                GuardarConfiguracion();
                CerrarVentanaConfiguracion();
            }           
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            CerrarVentanaConfiguracion();
        }        

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char c = Convert.ToChar(e.Text);
            e.Handled = !Char.IsNumber(c);

            base.OnPreviewTextInput(e);
        }        

        #endregion                                          

    }
}
