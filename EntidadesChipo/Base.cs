﻿
using System.Collections.Generic;
using System.ComponentModel;

namespace ChipoApplication.EntidadesChipo
{
    public class Base : INotifyPropertyChanged
    {

        #region Atributos

        private List<ConfiguracionMarcador> _listaDatosMarcadores;
        private bool _borrarArchivosTemporales;
        private string _archivoFuenteDatos;        
        private string _nombreArchivoFinal;        
        private string _nombrePlantilla;
        private string _rutaFinal;     

        #endregion
        #region Constructores
        #endregion
        #region Propiedades

        /// <summary>
        /// Propiedad que contiene la ruta y el nombre del archivo donde se obtendrá la información (excel)
        /// </summary>
        public string ArchivoFuenteDatos {
            get
            {
                return _archivoFuenteDatos;
            }
            set
            {
                _archivoFuenteDatos = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ArchivoFuenteDatos"));
            } 
        }
        /// <summary>
        /// Propiedad que contiene el valor que indica si se requiere eliminar los archivos temporales
        /// <value>Falso</value>
        /// </summary>
        public bool BorrarArchivosTemporales
        {
            get
            {
                return _borrarArchivosTemporales;
            }
            set
            {
                _borrarArchivosTemporales = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("BorrarArchivosTemporales"));
            }
        }
        /// <summary>
        /// Propiedad que contiene el nombre que se debe de agregar archivo final
        /// </summary>
        public string NombreArchivoFinal
        {
            get
            {
                return _nombreArchivoFinal;
            }
            set
            {
                _nombreArchivoFinal = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("NombreArchivoFinal"));
            }
        }
        /// <summary>
        /// Propiedad que contiene la ruta y el nombre del archivo final
        /// </summary>
        public string RutaNombreArchivoFinal
        {
            get
            {
                return RutaFinal + @"\" + NombreArchivoFinal + ".doc";
            }
        }
        
        /// <summary>
        /// Propiedad que contiene una lista de nombres de marcadores y su respectivo número de columna de donde se obtendrá su información
        /// </summary>
        public List<ConfiguracionMarcador> ListaConfiguracionDatosMarcadores
        {
            get
            {
                return _listaDatosMarcadores;
            }
            set
            {
                _listaDatosMarcadores = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ListaDatosMarcadores"));
            }
        }

        /// <summary>
        /// Propiedad que contiene el nombre de la plantilla de la cual se creará el archivo final
        /// </summary>
        public string NombrePlantilla
        {
            get
            {
                return _nombrePlantilla;
            }
            set
            {
                _nombrePlantilla = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("NombrePlantilla"));
            }
        }
        /// <summary>
        /// Propiedad requerida para aplicar la interfaz INotifyPropertyChangeds
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        public string RutaFinal
        {
            get
            {
                return _rutaFinal;
            }
            set
            {
                _rutaFinal = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("RutaFinal"));
            }
        }        
        
        #endregion
        #region Metodos
        #endregion
        #region Delegados

        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        #endregion  
                       
    }
}
