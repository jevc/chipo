﻿using System.ComponentModel;

namespace ChipoApplication.EntidadesChipo
{
    public class ConfiguracionMarcador : INotifyPropertyChanged
    {
        private string _nombre;
        private int _numeroColumna;

        public string Nombre
        {
            get
            {
                return _nombre;
            }
            set
            {
                _nombre = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Nombre"));
            }
        }

        public int NumeroColumna
        {
            get
            {
                return _numeroColumna;
            }
            set
            {
                _numeroColumna = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("NumeroColumna"));
            }
        }  

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }
    }
}
