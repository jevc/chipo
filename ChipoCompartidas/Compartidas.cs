﻿using System;

namespace ChipoApplication
{
    public class Compartidas
    {
        /// <summary>
        /// Método que libera el proceso que se inicia al ejecutar archivo ejecutable desde la aplicación
        /// </summary>
        /// <param name="obj"></param>
        public static void LiberarObjetos(object obj)
        {
            try
            {
                // libera el proceso
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
