﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using ChipoApplication.Controlador.Argumentos;
using ChipoApplication.EntidadesChipo;
using CtrlAdminArchivos;
using Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;
using Word = Microsoft.Office.Interop.Word;

namespace ChipoApplication.Controlador
{
    
    public delegate void DatosOrigenCargadosHandler(object sender, DatosOrigenCargadosArgs args);
    public delegate void ArchivoCreadoHandler(object sender, ArchivoCreadoArgs args);

    public class CtrlChipo
    {

        #region Atributos

        private List<string> _listaNombresArchivosTemp { get; set; }
        private List<string> _listaMensajesValidacion { get; set; }

        #endregion
        #region Constructores

         public CtrlChipo()
         {
             DatosGenerales = new Base();
             _listaNombresArchivosTemp = new List<string>();
             _listaMensajesValidacion = new List<string>();
         }

        #endregion
        #region Propiedades

        public Base DatosGenerales { get; set; }
        public List<List<Dictionary<int, string>>> ListaDatosOrigen { get; set; }        
               
        #endregion
        #region Eventos 

        public event DatosOrigenCargadosHandler DatosOrigenCargados;
        public event ArchivoCreadoHandler ArchivoCreado;

        #endregion
        #region Metodos

        private void BorrarArchivosTemporales()
        {
            ControladorArchivos.BorrarArchivos(_listaNombresArchivosTemp);            
        }

        private void CrearArchivoTemporal(List<Dictionary<int, string>> listaInfoArchivo)
        {
            object missing = System.Reflection.Missing.Value;
            object nombrePlantilla = DatosGenerales.NombrePlantilla;
            object nombreArchTemp = GenerarNombreAleatorio();
            string infoMarcador = string.Empty;
            Word.Application appWord = new Microsoft.Office.Interop.Word.Application();

            Document docWord = appWord.Documents.Open(ref nombrePlantilla, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
            
            foreach (ConfiguracionMarcador datosMarcador in DatosGenerales.ListaConfiguracionDatosMarcadores)
            {
                Range rng = docWord.Bookmarks.get_Item(datosMarcador.Nombre).Range;
               
                foreach (Dictionary<int, string> datoInfo in listaInfoArchivo)
                {
                    if (datoInfo.ContainsKey(datosMarcador.NumeroColumna)) infoMarcador = datoInfo[datosMarcador.NumeroColumna];
                }

                rng.Text = infoMarcador;                    
            }
                        
            docWord.SaveAs(ref nombreArchTemp,
                          ref missing, ref missing, ref missing, ref missing,
                          ref missing, ref missing, ref missing, ref missing,
                          ref missing, ref missing, ref missing, ref missing,
                          ref missing, ref missing, ref missing);


            docWord.Close(ref missing, ref missing, ref missing);
            appWord.Quit(ref missing, ref missing, ref missing);

            Compartidas.LiberarObjetos(docWord);
            Compartidas.LiberarObjetos(appWord);

            _listaNombresArchivosTemp.Add(nombreArchTemp.ToString());           
        }        

        private List<string> EsValidoGenerarArchivo()
        {            
            return ExisteArchivoFinal();
        }

        public bool EsValidoHabilitarGenerarArchivo()
        {
            bool esValidoGenerarArchivo =  !(string.IsNullOrEmpty(DatosGenerales.NombreArchivoFinal) ||
                            string.IsNullOrEmpty(DatosGenerales.NombrePlantilla) ||
                            string.IsNullOrEmpty(DatosGenerales.RutaFinal) ||
                            string.IsNullOrEmpty(DatosGenerales.ArchivoFuenteDatos));

            if(esValidoGenerarArchivo)
            {
                if (DatosGenerales.ListaConfiguracionDatosMarcadores == null) esValidoGenerarArchivo = false;
                else if (DatosGenerales.ListaConfiguracionDatosMarcadores.FindAll(c => c.NumeroColumna == 0).Count > 0) esValidoGenerarArchivo = false;

            }

            return esValidoGenerarArchivo;
        }

        private List<string > ExisteArchivoFinal()
        {
            if (File.Exists(DatosGenerales.RutaNombreArchivoFinal))
                _listaMensajesValidacion.Add("El archivo final ya existe en la carpeta destino. /n Elimine el archivo e intente de nuevo");
        
            return _listaMensajesValidacion;
        }

        public List<string> GenerarArchivo()
        {
            _listaMensajesValidacion = new List<string>();
            _listaNombresArchivosTemp = new List<string>();

            _listaMensajesValidacion = this.EsValidoGenerarArchivo();
            if (_listaMensajesValidacion.Count > 0) return _listaMensajesValidacion;

            ObtenerDatosArchivoOrigen();

            foreach (List<Dictionary<int, string>> listaDatosOrigen in ListaDatosOrigen)
            {
                CrearArchivoTemporal(listaDatosOrigen);
            }
          
            DatosGenerales.NombreArchivoFinal = ControladorArchivos.LimpiarNombreArchivoCaracteresNoValidos(DatosGenerales.NombreArchivoFinal);
            CombinarArchivosWord.CombinarArchivos.Combinar(_listaNombresArchivosTemp, DatosGenerales.RutaNombreArchivoFinal);

            if (DatosGenerales.BorrarArchivosTemporales)
                BorrarArchivosTemporales();

            return _listaMensajesValidacion;
        }

        private string GenerarNombreAleatorio()
        {
            return ControladorArchivos.GenerarNombreAleatorio(".doc", DatosGenerales.RutaFinal);
        }
       
        private void ObtenerDatosArchivoOrigen()
        {
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(DatosGenerales.ArchivoFuenteDatos);
            ListaDatosOrigen = new List<List<Dictionary<int, string>>>();
            List<Dictionary<int, string>> listaInfoFila;
            Excel.Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range range = xlWorksheet.UsedRange;
            Dictionary<int, string> infoColumna;
           
            int rCnt, cCnt;

            List<int> listaColumnas = this.DatosGenerales.ListaConfiguracionDatosMarcadores.Select(configuracionMarcador => configuracionMarcador.NumeroColumna).ToList();

            for (rCnt = 1; rCnt <= range.Rows.Count; rCnt++)
            {
                listaInfoFila = new List<Dictionary<int, string>>();
                for (cCnt = 1; cCnt <= range.Columns.Count; cCnt++)
                {
                    if(listaColumnas.Exists(c => c == cCnt))
                    {
                        infoColumna = new Dictionary<int, string>
                            { { cCnt, (string)(range.Cells[rCnt, cCnt] as Excel.Range).Value2 } };
                        listaInfoFila.Add(infoColumna);
                       
                    }                  
                }

                ListaDatosOrigen.Add(listaInfoFila);
            }

            xlWorkbook.Close(true, null, null);
            xlApp.Quit();

            Compartidas.LiberarObjetos(xlWorksheet);
            Compartidas.LiberarObjetos(xlWorkbook);
            Compartidas.LiberarObjetos(xlApp); 
           
            DatosOrigenCargadosArgs argDatosCargados = new DatosOrigenCargadosArgs(this.ListaDatosOrigen.Count);

            if(DatosOrigenCargados != null)
                DatosOrigenCargados(this, argDatosCargados);
        }

        public List<ConfiguracionMarcador> ObtenerMarcadoresDocumento()
        {
            if (DatosGenerales.ListaConfiguracionDatosMarcadores != null &&
                DatosGenerales.ListaConfiguracionDatosMarcadores.Count > 0) return DatosGenerales.ListaConfiguracionDatosMarcadores;

            List<ConfiguracionMarcador> listaMarcadores = new List<ConfiguracionMarcador>();
            object nombrePlantilla = DatosGenerales.NombrePlantilla;            
            object missing = System.Reflection.Missing.Value;
            
            if (nombrePlantilla == null) return listaMarcadores;

            Word.Application appWord = new Microsoft.Office.Interop.Word.Application();
            Document docWord = appWord.Documents.Open(ref nombrePlantilla, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);

            listaMarcadores = (from Bookmark marcador in docWord.Bookmarks select new ConfiguracionMarcador { Nombre = marcador.Name }).ToList();

            docWord.Close(ref missing, ref missing, ref missing);
            appWord.Quit(ref missing, ref missing, ref missing);

            Compartidas.LiberarObjetos(appWord);    
            Compartidas.LiberarObjetos(docWord);
                   
            return listaMarcadores;

        }
        #endregion
        #region Delegados
        #endregion                  
        
    }

   
}
