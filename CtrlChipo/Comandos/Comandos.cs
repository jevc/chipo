﻿using System.Windows.Input;

namespace ChipoApplication.Controlador.Comandos
{
    public static class Comandos
    {

        #region Atributos
        private static RoutedUICommand habilitarGenerarArchivo = new RoutedUICommand("HabilitarGenerarArchivo", "HabilitarGenerarArchivo", typeof(Comandos));
        private static RoutedUICommand habilitarConfigurarAplicacion = new RoutedUICommand("HabilitarConfigurarAplicacion", "HabilitarConfigurarAplicacion", typeof(Comandos));
        #endregion
        #region Constructores
        #endregion
        #region Propiedades

        public static RoutedUICommand HabilitarGenerarArchivo
        {
            get
            {
                return habilitarGenerarArchivo;
            }
        }

        public static RoutedUICommand HabilitarConfigurarAplicacion
        {
            get
            {
                return habilitarConfigurarAplicacion;
            }
        }

        #endregion
        #region Metodos
        #endregion
        #region Delegados
        #endregion  
        
    }
}
