﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ChipoApplication.Controlador;
using ChipoApplication.Controlador.Comandos;
using ChipoApplication.EntidadesChipo;

namespace ChipoApplication
{    

    /// <summary>
    /// Interaction logic for Chipo.xaml
    /// </summary>
    public partial class Chipo
    {

        #region Atributos

        private CtrlChipo _controlador;

        #endregion
        #region Constructores

        public Chipo()
        {
            InitializeComponent();            
            Iniciar();
        }

        #endregion
        #region Propiedades

        #endregion
        #region Metodos

        private void AgregarDelegados()
        {            
            btnSeleccionarRutaDestino.Click += btnSeleccionarRutaDestino_Click;
            btnSeleccionarArchivoOrigen.Click +=  btnSeleccionarArchivoOrigen_Click;
            btnSeleccionarArchivoFuente.Click += btnSeleccionarArchivoFuente_Click;
            btnGenerarArchivo.Click += btnGenerarArchivo_Click;
            btnConfigurar.Click += btnConfigurar_Click;

            ucConfigMarcadores.CerrarConfiguracion += ucConfigMarcadores_CerrarConfiguracion;
            ucConfigMarcadores.ConfiguracionActualizada += ucConfigMarcadores_ConfiguracionActualizada;
        }              

        private void GenerarArchivo()
        {
            
            List<string> listaMensajesValidacion = _controlador.GenerarArchivo();                                                
                       
            if (listaMensajesValidacion.Count > 0)
            {
                foreach (string msg in listaMensajesValidacion)
                {
                    MessageBox.Show(msg);
                }

                return;
            }            

            MessageBox.Show("El proceso ha finalizado");                        
        }
               

        private void Iniciar()
        {            
            this.InicializarControlador();
            
            IniciarComandos();            
            this.DataContext = _controlador.DatosGenerales;
            IniciarConfiguracion();
            AgregarDelegados();            
        }

        private void IniciarComandos()
        {
            btnGenerarArchivo.Command = Comandos.HabilitarGenerarArchivo;
            btnConfigurar.Command = Comandos.HabilitarConfigurarAplicacion;

            CommandBinding cmdHabilitarGenerarArchivo = new CommandBinding { Command = btnGenerarArchivo.Command };
            cmdHabilitarGenerarArchivo.CanExecute += cmdHabilitarGenerarArchivo_CanExecute;

            CommandBinding cmdHabilitarConfiguracion = new CommandBinding { Command = btnConfigurar.Command };
            cmdHabilitarConfiguracion.CanExecute += cmdHabilitarConfiguracion_CanExecute;

            CommandBindings.Add(cmdHabilitarConfiguracion);
            CommandBindings.Add(cmdHabilitarGenerarArchivo);
        }                

        private void IniciarConfiguracion()
        {
            ucConfigMarcadores.Iniciar(_controlador);
        }

        private void InicializarControlador()
        {
            _controlador = new CtrlChipo();
        }
        
        private void MostrarSeccionConfiguracion()
        {
            _controlador.DatosGenerales.ListaConfiguracionDatosMarcadores = _controlador.ObtenerMarcadoresDocumento();
            grdConfiguracion.DataContext = _controlador.DatosGenerales.ListaConfiguracionDatosMarcadores;
            grdConfiguracion.Visibility = Visibility.Visible;
        }

        private static void MostrarSelectorArchivo(TextBox txtVisorRuta, string extensionDefault, string filtro)
        {            
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();         
            dlg.DefaultExt = extensionDefault;
            dlg.Filter = filtro;
            
            bool? result = dlg.ShowDialog();

            if (result != true)
                return;
            
            string filename = dlg.FileName;
            txtVisorRuta.Text = filename;
            txtVisorRuta.Focus();
        }

        private static void MostrarSelectorRuta(TextBox txtVisorRuta)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();

            if(result == System.Windows.Forms.DialogResult.OK)
            {
                txtVisorRuta.Text = dialog.SelectedPath;
                txtVisorRuta.Focus();
            }
                
        }

        private void OcultarSeccionConfiguracion()
        {
            grdConfiguracion.Visibility = Visibility.Collapsed;
        }

        #endregion
        #region Delegados

        private void btnConfigurar_Click(object sender, RoutedEventArgs e)
        {
            MostrarSeccionConfiguracion();            
        }

        private void btnGenerarArchivo_Click(object sender, RoutedEventArgs e)
        {                        
            GenerarArchivo();
        }        

        private void btnSeleccionarArchivoFuente_Click(object sender, RoutedEventArgs e)
        {            
            MostrarSelectorArchivo(txtRutaArchivoFuente, ".xlsx", "Excel|*.xlsx; *.xls");            
        }       

        protected void btnSeleccionarArchivoOrigen_Click(object sender, RoutedEventArgs e)
        {
            MostrarSelectorArchivo(txtRutaArchivoOrigen, ".docx", "Word|*.doc; *.docx");
        }

        protected void btnSeleccionarRutaDestino_Click(object sender, RoutedEventArgs e)
        {
            MostrarSelectorRuta(txtRutaArchivoDestino);
        }

        private void cmdHabilitarConfiguracion_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !(string.IsNullOrEmpty(_controlador.DatosGenerales.NombrePlantilla));
        }

        private void cmdHabilitarGenerarArchivo_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _controlador.EsValidoHabilitarGenerarArchivo();
        }

        private void ucConfigMarcadores_ConfiguracionActualizada(object sender, System.EventArgs e)
        {
            
        }

        private void ucConfigMarcadores_CerrarConfiguracion(object sender, System.EventArgs e)
        {
            _controlador.DatosGenerales.ListaConfiguracionDatosMarcadores = grdConfiguracion.DataContext as List<ConfiguracionMarcador>;
            OcultarSeccionConfiguracion();
        }  

        #endregion  
                      
    }
}
